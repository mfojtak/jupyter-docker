#!/bin/bash

rm -rf $IPYTHONDIR
jupyter nbextension enable --py widgetsnbextension
ipcluster nbextension enable
ipython profile create --parallel --profile=mpi_local
mv /ipcluster_config.py $IPYTHONDIR/profile_mpi_local/ipcluster_config.py
tensorboard --logdir /data/tb_logs &
#cd theia
#yarn theia start /data --hostname=0.0.0.0 &
code-server $WORKSPACE_FOLDER --allow-http --no-auth -d $VSCODE_FOLDER &
xvfb-run -s "-screen 0 1400x900x24" jupyter notebook --allow-root --no-browser --ip 0.0.0.0 $*